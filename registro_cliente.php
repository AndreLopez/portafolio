<?php require_once('./Connections/db.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "signupform")) {
  $insertSQL = sprintf("INSERT INTO cliente (usuario, contrasena, fecha_nacimiento, nacionalidad, correo, nombre_publico, telefono_m, telefono_f, direccion, img, lvl) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['usuario'], "text"),
                       GetSQLValueString($_POST['c'], "text"),
                       GetSQLValueString($_POST['fecha_nacimiento'], "date"),
                       GetSQLValueString($_POST['nacionalidad'], "text"),
                       GetSQLValueString($_POST['correo'], "text"),
                       GetSQLValueString($_POST['nombre_publico'], "text"),
                       GetSQLValueString($_POST['telefono_m'], "text"),
                       GetSQLValueString($_POST['telefono_f'], "text"),
                       GetSQLValueString($_POST['direccion'], "text"),
                       GetSQLValueString($_POST['img'], "text"),
                       GetSQLValueString($_POST['lvl'], "text"));
  mysql_select_db($database_db, $db);
  $Result1 = mysql_query($insertSQL, $db) or die(mysql_error());
$correo = $_POST['correo'];
$_correo = base64_encode($correo);
  $insertGoTo = "./verify_cliente.php?c=".$_correo;
  if (isset($_SERVER['QUERY_STRING'])) {
    $insertGoTo .= (strpos($insertGoTo, '?')) ? "&" : "?";
    $insertGoTo .= $_SERVER['QUERY_STRING'];
  }
  header(sprintf("Location: %s", $insertGoTo));

}
mysql_select_db($database_db, $db);
$query_nuevo_cliente = "SELECT * FROM cliente";
$nuevo_cliente = mysql_query($query_nuevo_cliente, $db) or die(mysql_error());
$row_nuevo_cliente = mysql_fetch_assoc($nuevo_cliente);
$totalRows_nuevo_cliente = mysql_num_rows($nuevo_cliente);
?>
<?php
function sanear_string($string)
{
 
    $string = trim($string);
 
    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );
 
    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );
 
    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );
 
    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );
 
    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );
 
    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );
 
    return $string;
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <meta name="description" content="">
      <meta name="author" content="">

      <title>..::Portafolio::.</title>
     <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
     <link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic,900italic' rel='stylesheet' type='text/css'>
     <link href="https://db.onlinewebfonts.com/c/dffb2b80813d195d26fa4d2aad48b059?family=Gotham+Rounded" rel="stylesheet" type="text/css">

      <link rel="stylesheet" type="text/css" href="css/portafolio.css">
  </head>
  <body>
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">Portafolio</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a href="#inicio">Inicio</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#salaCompartida_Videos">Sala Compartida</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#salaCompartida_Videos">Videos</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">Fotos</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#services">Show</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="#">Cerca de Ti</a>
                    </li>
                   <!--  <li>
                        <a class="page-scroll" href="cerca-de-ti.html">Cerca de Ti</a>
                    </li> -->
                     <li>
                        <a class="page-scroll" href="chat.html">Chat</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="#">Compra Créditos</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="login.html">Ingresa</a>
                    </li>
                     <li>
                        <a class="page-scroll" href="registro-cliente.html">Únete</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <section>
        <div class="container">
        	<div class="registro col-lg-9 col-md-8 col-sm-12 col-xs-12">
      			<h1><span class="unete"> únete </span><span class="ahora">ahora mismo</span></h1>
  	    		<p>y comienza a disrutar de nuestros servicios</p>
  	    		<picture>
  	    			<img src="img/BLOQUE-REGISTRO-compressor.png">
  	    		</picture>
    		  

	        <div class="formulario col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        	<h1>¡registrate ya!</h1>
		        <form class="form-horizontal" role="form" method="POST" action="<?php echo $editFormAction; ?>" name="signupform" id="signupform" ">
		            <div class="row">
		              <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label for="usuario" class="col-md-6 control-label">USUARIO</label>
                      <div class="col-md-6">
                        <input type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuario" pattern="^[A-Za-z0-9]{6,}$"  title="Solo se permite nombre mayores a 6 caracteres y sin espacio" required>
                      </div>
                    </div>
                  </div>
                  <!--AQUI VA EL ALERT DE USUARIO-->
                  <div class="alert col-lg-4 col-md-12 col-sm-12 col-xs-12" id="result_usuario"></div>
                  <!--FIN ALERT DE USUARIO-->
                  <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label for="" class="col-md-6 control-label">CONTRASEÑA</label>
                      <div class="col-md-6">
                         <input name="c" type="password" class="form-control" id="c" placeholder="Contraseña"  pattern="[A-Za-z0-9!?-]{6,15}" maxlength="16" title="Estimado usuario, solo se permiten contraseñas mayores a 6 y menores a 15 caracteres. Solo números y letras   " required>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!--AQUI NO VA NADA--></div>

                  <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label for="" class="col-md-6 control-label">CONFIRMAR CONTRASEÑA</label>
                      <div class="col-md-6">
                        <input type="password" class="form-control" id="cc" name="cc" placeholder="Confirmar Contraseña" maxlength="16" required>
                      </div>
                    </div>
                  </div>
                  <!--AQUI VA EL ALERT DE CONFIRMAR CONTRASEÑA-->
                  <div class="alert col-lg-4 col-md-12 col-sm-12 col-xs-12" id="result_contrasena"></div>
                  <!--FIN ALERT DE CONFIRMAR CONTRASEÑA-->
                  <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label for="" class="col-md-6 control-label">CORREO ELECTRÓNICO</label>
                      <div class="col-md-6">
                         <input type="correo" class="form-control" name="correo" id="correo" placeholder="Correo  Eléctronico" id="correo" pattern="^[a-zA-Z0-9.!#$%'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"  title="mail@example.com"required >
                      </div>
                    </div>
                  </div>
                  <!--AQUI VA EL ALERT DE EMAIL-->
                  <div class="alert col-lg-4 col-md-12 col-sm-12 col-xs-12" id="result_correo"></div>
                  <!--FIN ALERT DE EMAIL-->
                  <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label for="" class="col-md-6 control-label">NOMBRE PÚBLICO</label>
                      <div class="col-md-6">
                        <input type="text" class="form-control" name="nombre_publico" placeholder="Nombre Publico " id="nombre_publico"  maxlength="16" pattern="[A-Za-z0-9!?- ]{6,15}" title="Ej. Lorenzo Mendoza"required>
                      </div>
                    </div>
                  </div> 
                  <!--AQUI VA EL ALERT DE NOMBRE PÚBLICO-->
                  <div class="alert col-lg-4 col-md-12 col-sm-12 col-xs-12" id="result_np"></div>
                  <!--FIN ALERT DE NOMBRE PÚBLICO-->
                  <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label for="" class="col-md-6 control-label">NACIONALIDAD</label>
                      <div class="col-md-6">
                        <select class="form-control" id="nacionalidad" name="nacionalidad" >
                           <?php
mysql_query('SET NAMES \'utf8\'');
$sql = mysql_query("SELECT paisnombre FROM pais ORDER BY paisnombre ASC");
while ($row = mysql_fetch_array($sql)){
$epilogo = sanear_string($row['paisnombre']);
echo "<option>" . $epilogo . "</option>";
}
?>
                        </select> 
                      </div>
                    </div>
                  </div>  
                  <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!--AQUI NO VA NADA--></div>
                  <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label for="" class="col-md-6 control-label">FECHA DE NACIMIENTO</label>
                      <div class="col-md-6">
                        <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" required>
                      </div>
                    </div>
                  </div>
                   <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!--AQUI NO VA NADA--></div>
                  <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label for="" class="col-md-6 control-label">TELÉFONO FIJO</label>
                      <div class="col-md-6">
                        <input type="tel" class="form-control" id="telefono_f" name="telefono_f" placeholder="Teléfono Fijo" required>
                      </div>
                    </div>
                  </div>
                   <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!--AQUI NO VA NADA--></div>
                  <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label for="" class="col-md-6 control-label">TELÉFONO MÓVIL</label>
                      <div class="col-md-6">
                        <input type="tel" class="form-control" id="telefono_m" name="telefono_m" placeholder="Teléfono Móvil" required>
                      </div>
                    </div>
                  </div>
                   <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!--AQUI NO VA NADA--></div>
                  <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                      <label for="" class="col-md-6 control-label">DIRECCIÓN</label>
                      <div class="col-md-6">
                        <textarea class="form-control" id="direccion" name="direccion" placeholder="Dirección de Residencia" rows="2" required></textarea>
                      </div>
                    </div>
                  </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12"><!--AQUI NO VA NADA--></div>
                  
		              <div class="col-sm-12 col-lg-12 col-md-12 col-xs-12">
		              	  <p> Al click en "UNIRSE AHORA", aceptas que posees más de 18 años de edad, además de que has leído, entendido y aceptado nuestros <a href="#">Términos y Condiciones</a></p>
		                  <button type="submit" id="btn-signup" value="UNIRSE AHORA" ondblclick="return aes();" onclick="return aes();" onsubmit="return aes();">UNIRSE AHORA
		                  </button>
                      <h4>¿Ya eres Miembro? <a href="login_cliente.php">Ingresa</a></h4>
                        <input name="img" type="hidden" id="img" value="./default.jpg">
              <input type="hidden" name="lvl" id="lvl" value="0">
               <input type="hidden" name="MM_insert" value="signupform">
		              </div>
		            </div><!-- /.row this actually does not appear to be needed with the form-horizontal -->
              
		        </form>
		    </div>
        </div>
        </div>
    </section>
    <footer>
<section id="about">
        <div id="footer_"> 
            <div class="row">
              <div class="col-xs-12 col-sm-4">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-6 col-sm-2">
                      <span class="btn boton_ingreso">
                      <p class="text-boton">ingreso modelo</p>
                      </span>
                    </div>
                    <div class="col-xs-6 col-sm-2">
                      <span class="btn boton_unete">
                      <p class="text-boton">únete a nosotros</p>
                      </span>
                    </div>
                  </div>
                </div>
              </div>

                <div class="container">
                  <div class="row" id="enlaces_horiz">
                    <div class="col-xs-12 col-sm-8">
                      <a href="" class="link">contáctanos</a>
                      <a href="" class="link">eventos</a>
                      <a href="" class="link">soporte online</a>
                      <a href="" class="link">F.A.Q</a>
                      <a href="" class="link">solicitud privada</a>
                    </div>
                  </div>
                </div>
            </div>

            <div class="row" id="footer_enlace">
              <div class="col-xs-12 col-sm-3 enlace">
              <a href="">inicio</a> <br>
              <a href="">sala compartida</a> <br>
              <a href="">videos</a> <br>
              <a href="">fotos</a> <br>
              <a href="">show</a> <br>
              <a href="">cerca de ti</a> <br>
              <a href="">chat</a> <br>
              <a href="">compra créditos</a>
              </div>

              <div class="col-sm-6">
                <!---<h1>EL LOGO VA AQUÍ</h1>-->
              </div>
            
            </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-12">
                <p id="texto_footer">El sitio contiene material sexual explícito. Entra sólo si eres mayor de 18 años de edad y si estás de acuerdo con nuestros términos y condiciones.</p>
              </div>
            
            </div>
          </div>
          <div id="piePag">
                <span class="copyright" style="color: #606468">Copyright 2016 &copy; ISMCENTER. Todos los Derechos Reservados.</span>
          </div>
        </div>            
    </section>
</footer>

 <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
      <!-- Theme JavaScript -->
    <script type='text/javascript' src='js/jquery-11.0.min.js'></script>  
      <script type="text/javascript">
    $(document).ready(function(){
                         
      var consul_usuario;
      var consul_correo;     
      var consul_usuario;
      $("#usuario").focus();
      $("#correo").focus();
      $("#nombre_publico").focus();
      $("#cc").focus();
     $("#usuario").keyup(function(e){
             
             consul_usuario = $("#usuario").val();
             $("#result_usuario").delay(500).queue(function(n) {      
                                           
                        $.ajax({
                              type: "POST",
                              url: "validar_for.php",
                              data: "usuario="+consul_usuario,
                              dataType: "html",
                              error: function(){
                                    alert("error petición ajax");
                                    return
                              },
                              success: function(data){
                                    $("#result_usuario").html(data);
                                    n();

                              }
                  });
                                           
             });
                                
      });

     $("#correo").keyup(function(e){
             
             consul_correo = $("#correo").val();
             $("#result_correo").delay(500).queue(function(n) {      
                                           
                        $.ajax({
                              type: "POST",
                              url: "validar_for.php",
                              data: "correo="+consul_correo,
                              dataType: "html",
                              error: function(){
                                    return
                              },
                              success: function(data){
                                    $("#result_correo").html(data);
                                    n();

                              }
                  });
                                           
             });
                                
      });
      
        $("#nombre_publico").keyup(function(e){
             
             consul_np = $("#nombre_publico").val();
             $("#result_np").delay(500).queue(function(n) {      
                                           
                        $.ajax({
                              type: "POST",
                              url: "validar_for.php",
                              data: "nombre_publico="+consul_np,
                              dataType: "html",
                              error: function(){
                                    return
                              },
                              success: function(data){
                                    $("#result_np").html(data);
                                    n();

                              }
                  });
                                           
             });
                                
      });
   $("#cc").keyup(function(e){
             
             consul_cc = $("#cc").val();
             consul_c = $("#c").val();
             $("#result_contrasena").delay(500).queue(function(n) {      
                                           
                        $.ajax({
                              type: "POST",
                              url: "validar_for.php",
                              data: "cc="+consul_cc + "&c=" + consul_c,
                              dataType: "html",
                              error: function(){
                                    return
                              },
                              success: function(data){
                                    $("#result_contrasena").html(data);
                                    n();

                              }
                  });
                                           
             });
                                
      });    
                      
});
</script>
<script>
function aes() {
if(usuario_compro==0 || correo_compro==0 || nombre_publico_compro==0 || contrasena_compro==0) {
alert("Verifique los Datos Introducidos en el Formulario");
return false;
} else {
return true;
}
}
</script> 
     

  </body>
</html>
<?php
mysql_free_result($nuevo_cliente);
?>


