<?php require_once('Connections/Integracion.php'); ?>
<?php
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  $theValue = (!get_magic_quotes_gpc()) ? addslashes($theValue) : $theValue;

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? "'" . doubleval($theValue) . "'" : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form1")) {
  $updateSQL = sprintf("UPDATE cliente SET usuario=%s, contrasena=%s, fecha_nacimiento=%s, pais=%s, ciudad=%s, correo=%s, nombre_publico=%s, telefono_m=%s, direccion=%s, telefono_f=%s, img=%s, lvl=%s WHERE id=%s",
                       GetSQLValueString($_POST['usuario'], "text"),
                       GetSQLValueString($_POST['contrasena'], "text"),
                       GetSQLValueString($_POST['fecha_nacimiento'], "date"),
                       GetSQLValueString($_POST['pais'], "text"),
                       GetSQLValueString($_POST['ciudad'], "text"),
                       GetSQLValueString($_POST['correo'], "text"),
                       GetSQLValueString($_POST['nombre_publico'], "text"),
                       GetSQLValueString($_POST['telefono_m'], "text"),
                       GetSQLValueString($_POST['direccion'], "text"),
                       GetSQLValueString($_POST['telefono_f'], "text"),
                       GetSQLValueString($_POST['img'], "text"),
                       GetSQLValueString($_POST['lvl'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_Integracion, $Integracion);
  $Result1 = mysql_query($updateSQL, $Integracion) or die(mysql_error());
}

mysql_select_db($database_Integracion, $Integracion);
$query_cliente = "SELECT * FROM cliente where id = 7";
$cliente = mysql_query($query_cliente, $Integracion) or die(mysql_error());
$row_cliente = mysql_fetch_assoc($cliente);
$totalRows_cliente = mysql_num_rows($cliente);

mysql_select_db($database_Integracion, $Integracion);
$query_nuevonivel = "SELECT * FROM updateniveltmp";
$nuevonivel = mysql_query($query_nuevonivel, $Integracion) or die(mysql_error());
$row_nuevonivel = mysql_fetch_assoc($nuevonivel);
$totalRows_nuevonivel = mysql_num_rows($nuevonivel);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
</head>

<body>
<table width="200" border="1">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

<form method="post" name="form1" action="<?php echo $editFormAction; ?>">
  <table align="center">
    <tr valign="baseline">
      <td nowrap align="right">Id:</td>
      <td><?php echo $row_nuevonivel['id']; ?></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Usuario:</td>
      <td><input type="text" name="usuario" value="" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Contrasena:</td>
      <td><input type="text" name="contrasena" value="" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Fecha_nacimiento:</td>
      <td><input type="text" name="fecha_nacimiento" value="" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Pais:</td>
      <td><input type="text" name="pais" value="" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Ciudad:</td>
      <td><input type="text" name="ciudad" value="" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Correo:</td>
      <td><input type="text" name="correo" value="" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Nombre_publico:</td>
      <td><input type="text" name="nombre_publico" value="" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Telefono_m:</td>
      <td><input type="text" name="telefono_m" value="" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Direccion:</td>
      <td><input type="text" name="direccion" value="" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Telefono_f:</td>
      <td><input type="text" name="telefono_f" value="" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Img:</td>
      <td><input type="text" name="img" value="" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">Lvl:</td>
      <td><input type="text" name="lvl" value="<?php echo $row_nuevonivel['idnivel']; ?>" size="32"></td>
    </tr>
    <tr valign="baseline">
      <td nowrap align="right">&nbsp;</td>
      <td><input type="submit" value="Actualizar registro"></td>
    </tr>
  </table>
  <input type="hidden" name="MM_update" value="form1">
  <input type="hidden" name="id" value="<?php echo $row_nuevonivel['id']; ?>">
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($cliente);

mysql_free_result($nuevonivel);
?>
