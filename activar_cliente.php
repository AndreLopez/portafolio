<?php require_once('./Connections/db.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "send")) {
  $updateSQL = sprintf("UPDATE cliente SET lvl=%s WHERE id=%s",
                       GetSQLValueString($_POST['lvl'], "text"),
                       GetSQLValueString($_POST['id'], "int"));

  mysql_select_db($database_db, $db);
  $Result1 = mysql_query($updateSQL, $db) or die(mysql_error());

  $updateGoTo = "./login_cliente.php";
  if (isset($_SERVER['QUERY_STRING'])) {
    $updateGoTo .= (strpos($updateGoTo, '?')) ? "&" : "?";
  }
  header(sprintf("Location: %s", $updateGoTo));
}

$colname_activar = "-1";
if (isset($_GET['u'])) {
	$var = $_GET['u'];
	$colname_activar = base64_decode($var);
}
mysql_select_db($database_db, $db);
$query_activar = sprintf("SELECT * FROM cliente WHERE usuario = %s", GetSQLValueString($colname_activar, "text"));
$activar = mysql_query($query_activar, $db) or die(mysql_error());
$row_activar = mysql_fetch_assoc($activar);
$totalRows_activar = mysql_num_rows($activar);
?>
<script>
window.onload = function(){
  document.forms['send'].submit()

}
</script>
<body>
<form action="<?php echo $editFormAction; ?>" method="POST" name="send" id="send">
<input name="lvl" type="hidden" value="1">
<input name="id" type="hidden" value="<?php echo $row_activar['id']; ?>">
<input type="hidden" name="MM_update" value="send">
</form>
</body>
<?php
mysql_free_result($activar);
?>
